import axios from 'axios';

const API_URL = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:3001';

export const getBook = async () => {
  const response = await axios.get(`${API_URL}/books`);
  return response.data;
};

export const updateBook = async (id, postData) => {
  const response = await axios.put(`${API_URL}/books/${id}`, postData);
  return response.data;
};
 