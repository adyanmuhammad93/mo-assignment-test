import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { closeModal, updateRow } from '@mo/store/tableSlice';

function Modal() {
  const showModal = useSelector(state => state.table.showModal);
  const selectedRowData = useSelector(state => state.table.selectedRowData);
  const dispatch = useDispatch();

  const [id, setId] = React.useState('');
  const [Title, setTitle] = React.useState('');
  const [Author, setAuthor] = React.useState('');
  const [Tags, setTags] = React.useState([]);

  React.useEffect(() => {
    if (selectedRowData) {
      setId(selectedRowData.id);
      setTitle(selectedRowData.Title);
      setAuthor(selectedRowData.Author);
      setTags(selectedRowData.Tags);
    }
  }, [selectedRowData]);

  const handleSubmit = event => {
    event.preventDefault();
    const newData = { Title, Author, Tags };
    console.log(newData)
    dispatch(updateRow({ id: selectedRowData.id, newData }));
    dispatch(closeModal());
  };

  const handleClose = () => {
    dispatch(closeModal());
  };

  if (!showModal) {
    return null;
  }

  return (
    <div className="fixed z-10 inset-0 overflow-y-auto">
      <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div className="fixed inset-0 transition-opacity" aria-hidden="true">
          <div className="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
        <div
          className="inline-block align-bottom bg-white rounded-lg px-4 pt-5 pb-4 text-left rtl:text-right overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full sm:p-6">
          <div>
            <div className="mt-3 sm:mt-5">
              <h3 className="text-lg leading-6 font-medium text-gray-900 mb-4">Edit Row</h3>
              <p className='text-slate-700 text-sm mb-4'>
                Please note that all changes will be reset when the browser refreshes!<br />
                It is just for test purposes, Thank You
              </p>
              <div className="mt-2">
                <form onSubmit={handleSubmit}>
                  <div className="grid grid-cols-1 gap-4">
                    <div className="col-span-1">
                      <label htmlFor="id" className="block font-medium text-gray-700 text-left rtl:text-right">
                        id, It's Non-Editable
                      </label>
                      <div className="mt-1 flex rounded-md shadow-sm">
                        <input
                          type="text"
                          name="id"
                          id="id"
                          value={id}
                          className="border rounded-lg py-2 px-3 w-full cursor-not-allowed"
                          disabled
                        />
                      </div>
                    </div>
                    <div className="col-span-1">
                      <label htmlFor="Title" className="block font-medium text-gray-700 text-left rtl:text-right">
                        Title
                      </label>
                      <div className="mt-1 flex rounded-md shadow-sm">
                        <input
                          type="text"
                          name="title"
                          id="Title"
                          value={Title}
                          onChange={e => setTitle(e.target.value)}
                          className="border rounded-lg py-2 px-3 w-full"
                        />
                      </div>
                    </div>
                    <div className="col-span-1">
                      <label htmlFor="Author" className="block font-medium text-gray-700 text-left rtl:text-right">
                        Author
                      </label>
                      <div className="mt-1 flex rounded-md shadow-sm">
                        <input
                          type="text"
                          name="author"
                          id="Author"
                          value={Author}
                          onChange={e => setAuthor(e.target.value)}
                          className="border rounded-lg py-2 px-3 w-full"
                        />
                      </div>
                    </div>
                    <div className="col-span-1">
                      <label htmlFor="Tags" className="block font-medium text-gray-700 text-left rtl:text-right">
                        Tags
                      </label>
                      <div className="mt-1 flex rounded-md shadow-sm">
                        <input
                          type="text"
                          name="tags"
                          id="Tags"
                          value={Tags}
                          onChange={e => setTags(e.target.value)}
                          className="border rounded-lg py-2 px-3 w-full"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="mt-5 sm:mt-6">
                    <button type="submit"
                      className="inline-flex justify-center w-full rounded-md border border-transparent shadow-sm px-4 py-2 bg-green-600 text-base font-medium text-white hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500 sm:text-sm">
                      Save
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="mt-5 sm:mt-6">
            <button type="button"
              className="inline-flex justify-center w-full rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500 sm:text-sm"
              onClick={handleClose}>
              Cancel
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Modal;