import React, { useEffect, useMemo, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { openModal, fetchTableData, setRTL, unsetRTL } from "@mo/store/tableSlice";
import {
  useTable,
  useSortBy,
  useFilters,
  usePagination,
  useGlobalFilter,
} from "react-table";

function GlobalFilter({
  preGlobalFilteredRows,
  globalFilter,
  setGlobalFilter,
}) {
  const count = preGlobalFilteredRows.length
  const [value, setValue] = React.useState(globalFilter)
  const onChange = (value) => {
    setGlobalFilter(value || undefined)
  }

  return (
      <input
        className="block w-full border-gray-300 drop-shadow focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border rounded-lg px-2 py-2"
        value={value || ""}
        onChange={e => {
          setValue(e.target.value);
          onChange(e.target.value);
        }}
        placeholder={`Search ${count} records...`}
        style={{
          fontSize: '1.1rem',
          border: '0',
        }}
      />
  )
}

function DefaultColumnFilter({
  column: { filterValue, preFilteredRows, setFilter },
}) {
  const count = preFilteredRows.length

  return (
    <input
    className="block w-full border-gray-300 drop-shadow focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border rounded-lg px-2 py-2"
      value={filterValue || ''}
      onChange={e => {
        setFilter(e.target.value || undefined) // Set undefined to remove the filter entirely
      }}
      placeholder={`Search ${count} records...`}
    />
  )
}

function Table({ title }) {
  const dispatch = useDispatch();
  const { columns, data, status, error } = useSelector((state) => state.table)
  const rtl = useSelector(state => state.table.rtl)

  // const columns = useMemo(
  //   () => [
  //     {
  //       Header: "User ID",
  //       accessor: "userId",
  //     },
  //     {
  //       Header: "ID",
  //       accessor: "id",
  //     },
  //     {
  //       Header: "Title",
  //       accessor: "title",
  //     },
  //   ],
  //   []
  // );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize, globalFilter },
    preGlobalFilteredRows,
    setGlobalFilter,
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0 }
    },
    useFilters,
    useGlobalFilter,
    useSortBy,
    usePagination
  );

  // useEffect(() => {
  //   dispatch(fetchTableData());
  // }, []);

  // if (status === "loading") {
  //   return <p>Loading...</p>;
  // }

  // if (status === "failed") {
  //   return <p>{error}</p>;
  // }

  const handleRowClick = (row) => {
    dispatch(openModal(row.original));
  };

  return (
    <>
      <div className="flex flex-col justify-evenly mx-auto px-4 max-w-7xl min-h-screen">
        <div className="my-4 w-full rtl:text-right">
          <h1 className="text-3xl">{title}</h1>
          <h1 className="text-sm mb-4">by Adyan Muhammad as Assignment Test for MengajiOnline.com</h1>
          <button 
          className={
            rtl 
            ? "bg-green-500 hover:bg-green-600 text-white font-semibold py-2 px-4 rounded-full mr-2 rtl:ml-2 opacity-30 cursor-not-allowed" 
            : "bg-green-500 hover:bg-green-600 text-white font-semibold py-2 px-4 rounded-full mr-2 rtl:ml-2"
          }
          onClick={() => dispatch(setRTL())}
          disabled={rtl}
        >
          RTL
        </button>
        <button 
          className={
            rtl 
            ? "bg-green-500 hover:bg-green-600 text-white font-semibold py-2 px-4 rounded-full" 
            : "bg-green-500 hover:bg-green-600 text-white font-semibold py-2 px-4 rounded-full opacity-30 cursor-not-allowed"
          }
          onClick={() => dispatch(unsetRTL())}
          disabled={!rtl}
        >
          LTR
        </button>
        </div>
        <div className="mb-4 w-full">
          <GlobalFilter
            preGlobalFilteredRows={preGlobalFilteredRows}
            globalFilter={globalFilter}
            setGlobalFilter={setGlobalFilter}
          />
        </div>
        <div className="grow mb-4">
          <div className="-my-2 overflow-x-auto">
            <div className="py-2 align-middle inline-block min-w-full">
              <div className="drop-shadow overflow-hidden border border-gray-200 sm:rounded-lg">
                <table
                  {...getTableProps()}
                  className="flex flex-col min-w-full divide-y divide-gray-200"
                >
                  <thead className="bg-gray-50">
                    {headerGroups.map((headerGroup) => (
                      <tr
                        {...headerGroup.getHeaderGroupProps()}
                        className="flex"
                      >
                        {headerGroup.headers.map((column) => (
                          <th
                          {...column.getHeaderProps(column.getSortByToggleProps())}
                            className="flex-1 p-4 text-left text-xs font-medium text-gray-500 uppercase tracking-wider cursor-pointer"
                          >
                            <div className="flex flex-col space-y-2">
                              <div className="flex rtl:flex-row-reverse">
                                <div className="grow">
                                  {column.render("Header")}
                                </div>
                                {column.isSorted
                                  ? column.isSortedDesc
                                    ? ' 🔽'
                                    : ' 🔼'
                                  : ' '}
                              </div>
                              <div>
                                {column.canFilter ? (
                                  <DefaultColumnFilter column={column} />
                                ) : null}
                              </div>
                            </div>
                          </th>
                        ))}
                      </tr>
                    ))}
                  </thead>
                  <tbody
                    {...getTableBodyProps()}
                    className="grow bg-white divide-y divide-gray-200"
                  >
                    {page.map((row) => {
                      prepareRow(row);
                      return (
                        <tr
                          {...row.getRowProps()}
                          onClick={() => handleRowClick(row)}
                          className="flex cursor-pointer hover:bg-gray-100"
                        >
                          {row.cells.map((cell) => {
                            return (
                              <td
                                {...cell.getCellProps()}
                                className="flex-1 py-3 truncate whitespace-nowrap"
                              >
                                <div className="flex items-center">
                                  <div className="ml-4 rtl:mr-4">
                                    <div className="text-sm font-medium text-gray-900">
                                      {cell.render("Cell")}
                                    </div>
                                  </div>
                                </div>
                              </td>
                            );
                          })}
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div className="flex items-center justify-between my-4 shadow overflow-hidden border-b border-gray-200 sm:rounded-lg bg-gray-50 text-xs font-medium text-gray-500 uppercase">
          <div className="flex items-center p-4">
            <span className="mr-2">Rows per page:</span>
            <select
              className="border py-1 px-3 rounded-lg"
              value={pageSize}
              onChange={e => {
                setPageSize(Number(e.target.value))
              }}
            >
              {[10, 25, 50, 100].map((pageSize) => (
                <option key={pageSize} value={pageSize}>
                  {pageSize}
                </option>
              ))}
            </select>
          </div>
          <div className="flex items-center p-4">
            <span className="mr-2">
              Page {pageIndex + 1} of {pageOptions.length}
            </span>
            <button
              className="bg-gray-300 hover:bg-gray-400 rounded-l-lg py-2 px-4 mr-2 disabled:opacity-30"
              onClick={() => gotoPage(0)}
              disabled={!canPreviousPage}
            >
              {"<<"}
            </button>
            <button
              className="bg-gray-300 hover:bg-gray-400 py-2 px-4 mr-2 disabled:opacity-30"
              onClick={() => previousPage()}
              disabled={!canPreviousPage}
            >
              {"<"}
            </button>
            <button
              className="bg-gray-300 hover:bg-gray-400 py-2 px-4 mr-2 disabled:opacity-30"
              onClick={() => nextPage()}
              disabled={!canNextPage}
            >
              {">"}
            </button>
            <button
              className="bg-gray-300 hover:bg-gray-400 rounded-r-lg py-2 px-4 mr-2 disabled:opacity-30"
              onClick={() => gotoPage(pageCount - 1)}
              disabled={!canNextPage}
            >
              {">>"}
            </button>
          </div>
        </div>
      </div>
    </>
  );
}

export default Table;
