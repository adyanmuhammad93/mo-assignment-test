import '@mo/styles/globals.css'
import React from 'react';
import { Provider } from 'react-redux';
import store from '@mo/store';

export default function App({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  )
}