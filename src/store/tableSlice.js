import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { getBook, updateBook } from '@mo/utils/api';
import books from '../../db.json'

console.log(books)

const initialState = {
  columns: [
    {
      Header: 'Id',
      accessor: 'id',
    },
    {
      Header: 'Title',
      accessor: 'Title',
    },
    {
      Header: 'Author',
      accessor: 'Author',
    },
    {
      Header: 'Tags',
      accessor: 'Tags',
    },
  ],
  data: books.books,
  // status: 'idle',
  // error: null,
  showModal: false,
  selectedRowData: null,
  rtl: false
};

// Async thunk to fetch data from JSONPlaceholder API
// export const fetchTableData = createAsyncThunk('table/fetchTableData', async () => {
//   const response = await getBook()
//   const data = await response
//   return data;
// });

const tableSlice = createSlice({
  name: 'table',
  initialState,
  reducers: {
    openModal(state, action) {
      state.showModal = true;
      state.selectedRowData = action.payload;
    },
    closeModal(state) {
      state.showModal = false;
      state.selectedRowData = null;
    },
    updateRow(state, action) {
      const { Id, newData } = action.payload;
      const index = state.data.findIndex(row => row.Id === Id);
      state.data[index] = { ...state.data[index], ...newData };
    },
    setRTL(state) {
      state.rtl = true
    },
    unsetRTL(state) {
      state.rtl = false
    }
  },
  // extraReducers: builder => {
  //   builder
  //     .addCase(fetchTableData.pending, state => {
  //       state.status = 'loading';
  //     })
  //     .addCase(fetchTableData.fulfilled, (state, action) => {
  //       state.status = 'succeeded';
  //       state.data = action.payload;
  //     })
  //     .addCase(fetchTableData.rejected, (state, action) => {
  //       state.status = 'failed';
  //       state.error = action.error.message;
  //     });
  // },
});

export const { openModal, closeModal, updateRow, setRTL, unsetRTL } = tableSlice.actions;

export default tableSlice.reducer;
